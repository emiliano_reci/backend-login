#Backend
Api cuenta con tres rutas (signup, login, me)
**Signup**: Almacena en la bd un usuario, con la condicion que el mail sea unico; y luego genera un token que junto con el usuario será la respuesta.
**Login**: Verifica el mail y password, en caso de ser verificado exitosamente genera un token y devuelve como respuesta el usuario y el token.
**Me**: Ésta ruta cuenta con un middleware"Auth", el cual verifica que el token que envía el frontend sea válido  y luego lo busca en la bd, en caso de encontrarlo devuelve los datos del usuario.

Las variables entorno están el archivo .env

##Pasos para levantar el backend:
Se debe levantar mongodb con el primer script, dependiendo como este instalado, lo que es posible que el script sea mongod en vez de solamente mongo.
Instalar las dependencias/librerias con el segundo script
Luego levantar el backend, que en modo de desarrollo es con el tercer script. En caso de levantarlo en produccion con el cuarto script.



##scripts:
    mongo
    npm install
    npm run dev
    npm start



librerias:
    *Validator: verifica el mail.
    *dotenv: variables entornos.
    *bcryptjs: encripta el pass.


