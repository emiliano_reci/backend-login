const mongoose = require("mongoose");
const bcript = require("bcryptjs");
const jwt = require("jsonwebtoken");
const validator = require("validator");
const Schema = mongoose.Schema;

const UserSchema = Schema({
  name: {
    type: String,
    required: true,
    trim: true,
    minlength: 3,
  },
  email: {
    type: String,
    unique: true,
    trim: true,
    required: true,
    lowercase: true,
    validate(value) {
      if (!validator.isEmail(value)) throw new Error("email invalido");
    },
  },
  password: {
    type: String,
    required: true,
    trim: true,
    minlength: 2,
  },
  tokens: [
    {
      token: {
        type: String,
        require: true,
      },
    },
  ],
});

//  Quito el pass y el array de tokens para que no se envie al frontend, éstos datos.
UserSchema.methods.toJSON = function () {
  const user = this;
  const userObject = user.toObject();
  delete userObject.password;
  delete userObject.tokens;

  return userObject;
};

//  Generar token de autenticacion en el login.
UserSchema.methods.generateAuthToken = async function () {
  const user = this;
  const token = jwt.sign({ _id: user._id.toString() }, process.env.JWT_SECRET);
  user.tokens = user.tokens.concat({ token: token });
  await user.save();
  return token;
};

/*
 **  Uso del middleware pre(), se ejecuta antes de "save" tanto para cuando se
 **  crea o modifica un usuario. Encripta el password del cliente, antes de guardarlo en la bd.
 */
UserSchema.pre("save", async function (next) {
  //  hace referecia al documento, "los datos del usuario".
  const user = this;

  //  console.log('desde el middleware... antes de save')

  if (user.isModified("password")) {
    user.password = await bcript.hash(user.password, 8);
  }

  //  para terminar el middleware y pasar a lo siguiente
  next();
});

//  Creando metodo findByCredentials.
UserSchema.statics.findByCredentials = async (email, pass) => {
  const user = await User.findOne({ email: email });
  if (!user) {
    throw new Error("No se puede loguear");
  }

  const isMatch = await bcript.compare(pass, user.password);

  if (!isMatch) {
    throw new Error("No se puede loguear...");
  }

  return user;
};

const User = mongoose.model("User", UserSchema);

module.exports = User;
