const express = require("express");
const userCtrl = require("../controllers/user");
const auth = require("../middleware/authentication");
const api = express.Router();

//**********Routes User ***********

api.post("/user/signup", userCtrl.saveUser);

api.post("/user/login", userCtrl.loginUser);

api.get("/user/me", auth, userCtrl.profileUser);

module.exports = api;
