const User = require("../models/User");

//  Signup user: Guarda un usuario y genera un token.
const saveUser = async (req, res) => {
  const user = new User(req.body);

  try {
    await user.save();
    const token = await user.generateAuthToken();
    res.send({ user, token });
  } catch (error) {
    res.status(400).send(error);
  }
};

//  Login de usuario: Verifica el mail y pass, y genera un token.
const loginUser = async (req, res) => {
  try {
    const email = req.body.email;
    const pass = req.body.password;
    //findByCredentials: no es propio de mongoose, esta creada en el model/users.
    const user = await User.findByCredentials(email, pass);
    const token = await user.generateAuthToken();
    res.send({ user, token });
  } catch (error) {
    res.status(400).send(error);
  }
};

//  Ruta Protegiga, con middleware "auth".
const profileUser = async (req, res) => {
  res.send(req.user);
};

module.exports = {
  saveUser,
  loginUser,
  profileUser,
};
